import os
import argparse

import tensorflow as tf
import numpy as np

import config
from utils import log
from models.WorldModel.VAE import VAE

ROOT_DIR_NAME = './data/'
ROLLOUT_DIR_NAME = './data/rollout/'
SERIES_DIR_NAME = './data/series/'


def get_filelist(N, train_dir):
    filelist = os.listdir(train_dir)
    filelist = [x for x in filelist if x != '.DS_Store']
    filelist.sort()
    length_filelist = len(filelist)


    if length_filelist > N:
        filelist = filelist[:N]

    if length_filelist < N:
        N = length_filelist

    return filelist, N

def encode_episode(vae, episode):

    obs = episode['obs']
    action = episode['action']
    reward = episode['reward']
    done = episode['done']

    done = done.astype(int)  
    reward = np.where(reward>0, 1, 0) * np.where(done==0, 1, 0)

    mu, log_var = vae.encoder_mu_log_var.predict(obs)
    
    initial_mu = mu[0, :]
    initial_log_var = log_var[0, :]

    return (mu, log_var, action, reward, done, initial_mu, initial_log_var)



def generate_rnn_data(N=1000, vae_file=None, rnn_file=None, save_folder=None):
    N = int(N)

    tf.compat.v1.disable_eager_execution()
    vae = VAE()

    if vae_file:
        vae_loadpath = './savedModels/vae/' + vae_file
        log(f'Loading vae from {vae_loadpath}')
    else:
        vae_loadpath = './savedModels/vae/weights.h5'

    try:
        vae.set_weights(vae_loadpath)
    except:
        log(f"{vae_loadpath} does not exist - ensure you have run train_vae.py first")
        raise


    if save_folder:
        log('Save folder specified')
        train_dir = ROLLOUT_DIR_NAME + save_folder + '/'
        save_dir = SERIES_DIR_NAME + save_folder + '/'
        root_dir = ROOT_DIR_NAME + save_folder + '/'
        log(f'Training directory: {train_dir}')
        log(f'Save directory: {save_dir}')
        log(f'Root directory: {root_dir}')
    else:
        train_dir = ROLLOUT_DIR_NAME
        save_dir = SERIES_DIR_NAME
        root_dir = ROOT_DIR_NAME

    log("Getting filelist")
    filelist, N = get_filelist(N, train_dir)

    file_count = 0

    initial_mus = []
    initial_log_vars = []

    for file in filelist:
        try:
            log("Loading rollout data for episode {}".format(file))
            rollout_data = np.load(train_dir + file, allow_pickle=True)

            log("Encoding episode")
            mu, log_var, action, reward, done, initial_mu, initial_log_var = encode_episode(vae, rollout_data)

            np.savez_compressed(save_dir + file, mu=mu, log_var=log_var, action = action, reward = reward, done = done)
            initial_mus.append(initial_mu)
            initial_log_vars.append(initial_log_var)

            file_count += 1

            if file_count%50==0:
                log('Encoded {} / {} episodes'.format(file_count, N))

        except Exception as e:
            log(e)
            log('Skipped {}...'.format(file))

    log('Encoded {} / {} episodes'.format(file_count, N))

    initial_mus = np.array(initial_mus)
    initial_log_vars = np.array(initial_log_vars)

    log('ONE MU SHAPE = {}'.format(mu.shape))
    log('INITIAL MU SHAPE = {}'.format(initial_mus.shape))

    np.savez_compressed(root_dir + 'initial_z.npz', initial_mu=initial_mus, initial_log_var=initial_log_vars)

    
if __name__ == "__main__":
    os.chdir('/nfs')
    parser = argparse.ArgumentParser(description=('Generate RNN data'))
    parser.add_argument('--N',default = 1000, help='number of episodes to use to train')
    parser.add_argument('--vae',help='The filename of the VAE to use for encoding')
    parser.add_argument('--save_folder', help='the subfolder containing the training data')
    parser.add_argument('--rnn_file', help='The file to save the RNN in')
    args = parser.parse_args()

    generate_rnn_data(args.N, args.vae, args.rnn_file, args.save_folder)