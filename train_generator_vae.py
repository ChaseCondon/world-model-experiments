import os
import argparse

import numpy as np 
import tensorflow as tf

import config
from utils import log
from models.WorldModel.VAE_gen import VAE


DIR_NAME = './data/rollout/'
M = 1500
SCREEN_SIZE_X = 200
SCREEN_SIZE_Y = 200

def import_data(N, chunk_size):
    N = int(N)

    filelist = os.listdir(DIR_NAME)
    filelist = [x for x in filelist if x != '.DS_Store']
    filelist.sort()
    length_filelist = len(filelist)

    # log("{} training files found".format(length_filelist))

    if length_filelist > N:
        filelist = filelist[:N]
    
    if length_filelist < N:
        N = length_filelist

    for i in range(0, N, chunk_size):
        num_samples = M*chunk_size

        data = np.zeros((num_samples, SCREEN_SIZE_X, SCREEN_SIZE_Y, 3), dtype=np.float32)
        idx = 0

        for file in filelist[i:i+chunk_size]:
            try:
                new_data = np.load(DIR_NAME + file, allow_pickle=True)['obs']
                
                data[idx:(idx + M), :, :, :] = new_data

                idx = idx + M

            except Exception as e:
                log('Skipped {} ::: {}'.format(file, e))
    
        yield (data, data)


def train_vae(N=10000, new_model=True, epochs=10, chunk_size=10, vae=None):
    N = int(N)
    epochs = int(epochs)

    tf.compat.v1.disable_eager_execution()
    vae = vae or VAE()

    if not new_model:
        try:
            vae.set_weights('./savedModels/vae/weights.h5')
        except:
            log('Ensure ./vae/weights.h5 exists')
            raise

    try:
        log("Begin loading training data")
        data_generator = import_data(N, chunk_size)
    except:
        log('NO DATA FOUND')
        raise
    
    # log('DATA SHAPE = {}'.format(data.shape))

    for epoch in range(epochs):
        log('EPOCH ' + str(epoch))
        steps = chunk_size*M
        vae.train(data_generator, steps)
        vae.save_weights('./savedModels/vae/weights.h5')

if __name__ == "__main__":
    os.chdir('/nfs')
    
    parser = argparse.ArgumentParser(description=('Train VAE'))
    parser.add_argument('--N', type=int, default = 1000, help='number of episodes to use to train')
    parser.add_argument('--new_model', action='store_true', help='start a new model from scratch?')
    parser.add_argument('--epochs', type=int, default = 25, help='number of epochs to train for')
    parser.add_argument('--chunk_size', type=int, default=100, help='number of episodes per traning chunk')
    args = parser.parse_args()

    if args.N % args.chunk_size != 0:
        log('Number of samples (N) must be evenly divisible by the chunk size')
        exit(1)

    log("Begin training VAE")
    train_vae(args.N, args.new_model, args.epochs, args.chunk_size)