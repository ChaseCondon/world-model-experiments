import os
import argparse
from collections import deque
from datetime import datetime

import gym
import vizdoomgym
import numpy as np

"""
Report on training stats
"""
def report_training(training_id,
                    episode,
                    episode_reward,
                    total_reward,
                    average_reward,
                    last_average,
                    no_append=False):
    report = "{date} - Episode {episode}:\
               \n\tlatest episode reward: {episode_reward}\
               \n\ttotal episode reward: {total_reward}\
               \n\taverage_reward: {average_reward}\
               \n\tchange in average: {average_change}".format(
                    date = datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
                    episode = episode,
                    episode_reward = episode_reward,
                    total_reward = total_reward,
                    average_reward = average_reward,
                    average_change = (average_reward - last_average),
                )

    if (episode-1)%10 == 0:
        print(report, flush=True)
    if not no_append:
        with open("random-out-{}.log".format(training_id), 'a') as file:
            file.write(report + '\n')

"""
Run a random rollout on the environment.
"""
def train(training_id):
    # Change directory to appease the cluster gods.
    # os.chdir('/nfs')
    env = gym.make('VizdoomTakeCover-v0')

    total_reward = 0            # The total reward across all episodes
    average_reward = 0          # The rolling average reward across all episodes
    last_average = 0            # Rolling average at the previous episode (for testing convergence)
    episode_rewards = []        # The rewards by episode
    total_step = 0              # The total number of steps taken across all episodes (needed to track when to train the model.)
    max_episode = 1000000
    max_step = 5000
    episode = 0
    
    while episode < max_episode:

        episode += 1
        episode_reward = 0
        state = env.reset()

        for t_step in range(max_step):
            total_step += 1

            action_idx = env.action_space.sample()
            try:
                next_state, reward, done, info = env.step(action_idx)
            except:
                print("VizDoom crashed. Restarting\n", flush=True)
                total_reward += episode_reward
                break
        
            episode_reward += reward

            if t_step == max_step or done:
                total_reward += episode_reward
                episode_rewards.append(episode_reward)
                break

        # Check change in rolling average for convergence and report
        if len(episode_rewards) > 1:
            average_reward = np.mean(episode_rewards[:-1])
        
        report_training(training_id, episode, episode_reward, total_reward, 
                        average_reward, last_average)

        last_average = average_reward

    env.close()
    

if __name__ == '__main__':
    os.chdir('/nfs')

    training_id = datetime.now().strftime('%d%m%Y-%H%M%S')
    with open("random-out-{}.log".format(training_id), 'w') as file:
        file.write("Beginning log for training: {}\n".format(training_id))
    train(training_id)