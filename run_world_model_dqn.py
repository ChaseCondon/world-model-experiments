import os
import argparse

import numpy as np
import gym
import vizdoomgym

from models.WorldModel import RNN, VAE
from generate_data import generate_data
from train_vae import train_vae
from generate_rnn_data import generate_rnn_data
from train_rnn import train_rnn

class WorldModel():

    def __init__(self):
        self.VAE = VAE()
        self.RNN = RNN()

    def train(self, num_episodes=10000, controller=None):
        print('Beginning World Model training', flush=True)

        print('Generating training data...', flush=True)
        generate_data(num_episodes, controller=controller)

        print('Training VAE...', flush=True)
        train_vae(num_episodes, epochs=30, vae=self.VAE)

        print('Generating RNN training data...', flush=True)
        generate_rnn_data(num_episodes, vae=self.VAE)

        print('Training RNN...', flush=True)
        train_rnn(num_episodes, rnn=self.RNN)


class WorldModelDQNAgent():
    def __init__(self, state_size, 
                 action_size,
                 gamma=0.99,
                 learning_rate=0.0005,
                 batch_size=32):
        self.state_size = state_size
        self.action_size = action_size 

        self.gamma = gamma
        self.learning_rate = learning_rate
        self.epsilon = 1.0
        self.initial_epsilon = 1.0
        self.epsilon_decay = .999
        self.final_epsilon = 0.01
        self.observe = 50000
        self.timestep_per_train = 100
        self.batch_size = batch_size
        
        self.replay_buffer = ReplayBuffer(self.observe)
        self.buffer_min_size = 10000

        self.model = self.create_model(self.state_size, self.action_size, self.learning_rate)
        self.target_model = self.create_model(self.state_size, self.action_size, self.learning_rate)


if __name__ == "__main__":
    os.chdir('/nfs')

    parser = argparse.ArgumentParser(description=('Run the whole world model'))
    parser.add_argument('--train', action='store_true', help='train the whole world model')
    parser.add_argument('--N', default = 10000, help='number of episodes to use to train')
    args = parser.parse_args()

    training = args.train
    num_episodes = args.N

    model = WorldModel()

    if training:
        model.train(num_episodes)