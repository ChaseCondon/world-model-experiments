import os
import argparse

import numpy as np 
import tensorflow as tf

import config
from utils import log
from models.WorldModel.VAE import VAE


DIR_NAME = './data/rollout/'
M = 1500
SCREEN_SIZE_X = 200
SCREEN_SIZE_Y = 200

def import_data(training_size, batch_size, image_folder=None):
    training_size = int(training_size)

    if image_folder:
        log(f"Loading images from {DIR_NAME + image_folder}")
        filepath = DIR_NAME + image_folder + '/'
    else:
        log(f"Loading images from {DIR_NAME}")
        filepath = DIR_NAME

    filelist = os.listdir(filepath)
    filelist = [x for x in filelist if x != '.DS_Store']
    filelist.sort()
    length_filelist = len(filelist)

    log("{} training files found".format(length_filelist))

    # if length_filelist > N:
    #     filelist = filelist[:N]
    
    # if length_filelist < N:
    #     N = length_filelist
    
    data = np.zeros((training_size, 200, 200, 3))
    file_count = 0
    size = 0

    for file in filelist:
        try:
            new_data = np.load(filepath + file, allow_pickle=True)['obs']
            num_obs = len(new_data)

            if size + num_obs > training_size:
                num_obs = training_size-size
                new_data = new_data[:num_obs]

            data[size:size+num_obs] = new_data

            size += num_obs
            if size >= training_size:
                break
            file_count += 1

            if file_count%50 == 0:
                log('Imported {}/{} ::: Current data size = {} observations'.format(file_count, len(filelist), size))
        except Exception as e:
            log('Skipped {} ::: {}'.format(file, e))
    
    data = data[:size]
    log('Imported {}/{} ::: Current data size = {} observations'.format(file_count, len(filelist), len(data)))

    np.random.shuffle(data)
    if size != training_size:
        size -= size % batch_size
        data = data[:size]
        while size > training_size:
            size -= batch_size
            data = data[:size]
        
        log('Data trimmed to {} observations'.format(size))

    return data, training_size

def train_vae(training_size=100000, new_model=True, file_path=None, image_folder=None, epochs=10, batch_size=100, vae=None):
    training_size = int(training_size)
    epochs = int(epochs)

    tf.compat.v1.disable_eager_execution()
    vae = vae or VAE()

    if not new_model:
        try:
            if file_path:
                vae.set_weights('./savedModels/vae/' + file_path)
            else:
                vae.set_weights('./savedModels/vae/weights.h5')
        except:
            log('Ensure ./vae/weights.h5 exists')
            raise

    try:
        log("Begin loading training data")
        data, training_size = import_data(training_size, batch_size, image_folder)
    except:
        log('NO DATA FOUND')
        raise
    
    log('DATA SHAPE = {}'.format(data.shape))

    for epoch in range(epochs):
        log('EPOCH ' + str(epoch))
        vae.train(data, batch_size)
        if file_path:
            vae.save_weights('./savedModels/vae/' + file_path)
        else:
            vae.save_weights('./savedModels/vae/weights.h5')

if __name__ == "__main__":
    os.chdir('/nfs')

    parser = argparse.ArgumentParser(description=('Train VAE'))
    parser.add_argument('--training_size', type=int, default=1000000, help='number of episodes to use to train')
    parser.add_argument('--new_model', action='store_true', help='start a new model from scratch?')
    parser.add_argument('--image_folder', help='Where the training images are stored')
    parser.add_argument('--file_path', help='Where to store / load the model')
    parser.add_argument('--epochs', type=int, default=20, help='number of epochs to train for')
    parser.add_argument('--batch_size', type=int, default=100, help='training batch size')
    args = parser.parse_args()

    log("Begin training VAE")
    train_vae(args.training_size, args.new_model, args.file_path, args.image_folder, args.epochs, args.batch_size)