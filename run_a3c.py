import os
import argparse 

import gym
import vizdoomgym
import numpy as np 

from models.ActorCritic.environment import Environment
from models.ActorCritic.A3C import A3CAgent

parser = argparse.ArgumentParser(description='Run deep Q-network on the game VizDoom: Take Cover.')
parser.add_argument('--train', dest='train', action='store_true', help='Train the model.')
parser.add_argument('--max-eps', default=100000, type=int, help='Global maximum number of episodes to run.')
parser.add_argument('--n-threads', default=8, type=int, help='Number of parallel worker threads.')
parser.add_argument('--lr', default=0.0002, help='Learning rate for the shared optimizer.')
parser.add_argument('--gamma', default=0.99, help='Discount factor of rewards.')
parser.add_argument('--save-dir', default='/nfs/saved_models/', type=str, help='Directory in which to save the model.')
args = parser.parse_args()

if __name__ == '__main__': 
    os.chdir('/nfs')

    env = gym.make('VizdoomTakeCover-v0')
    e = Environment(env, 4)
    agent = A3CAgent(e.get_state_size(), e.get_action_size(), args.n_threads, args.lr, args.gamma)

    if args.train:
        agent.train(env, max_episode=args.max_eps)
    else:
        print('Add a test mode ya fool!')