import skimage as skimage
from skimage import transform, color, exposure
import numpy as np
import random

def generate_data_action(t, env):

    a = env.action_space.sample()
    return a


def adjust_obs(obs, env):
    if env == 'VizdoomTakeCover-v0':
        crop_obs = obs[80:-10,30:-30]
    elif env == 'CartPole-v0':
        crop_obs = obs[150:,:]
    elif env == 'MsPacman-v0':
        crop_obs = obs[172:,:]
    else:
        crop_obs = obs[25:-10,:]

    normalized_obs = crop_obs/255.0
    preprocessed_obs = transform.resize(normalized_obs, [200, 200, 3])
    return preprocessed_obs
    

def adjust_reward(reward):
    if reward > 0:
        reward = 1
    else:
        reward = 0
    return reward