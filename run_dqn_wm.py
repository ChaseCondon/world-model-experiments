import os
import pickle
import resource
import argparse
from collections import deque
from datetime import datetime

import gym
import vizdoomgym
import numpy as np

from config import adjust_obs
from models.WorldModel.VAE import VAE
from models.WorldModel.RNN import RNN
from models.WorldModel.DQN import DQNAgent
from models.ReplayBuffer import ReplayBuffer

logs = []

"""
Report on training stats
"""
def report_training(save_file,
                    episode,
                    episode_reward,
                    total_reward,
                    average_reward,
                    last_average,
                    avg_Q_loss,
                    no_append=False):
    report = "{date} - Episode {episode}:\
               \n\tlatest episode reward: {episode_reward}\
               \n\ttotal episode reward: {total_reward}\
               \n\taverage_reward: {average_reward}\
               \n\tchange in average: {average_change}\
               \n\taverage Q_loss: {avg_Q_loss}".format(
                    date = datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
                    episode = episode,
                    episode_reward = episode_reward,
                    total_reward = total_reward,
                    average_reward = average_reward,
                    average_change = (average_reward - last_average),
                    avg_Q_loss = avg_Q_loss
                )

    if (episode-1)%args.report_step == 0:
        print(report, flush=True)
    if not no_append:
        with open(f'{save_file}.log', 'a') as file:
            file.write(report + '\n')


def get_obs_encoding(vae, rnn, obs, action, reward, rnn_hidden, rnn_cell_values):
    obs = adjust_obs(obs, args.env)
    vae_encoded_obs = vae.encoder.predict(np.array([obs]))[0]

    action = np.array([action])
    input_to_rnn = [np.array([[np.concatenate([vae_encoded_obs, action, [reward]])]]),
                    np.array([rnn_hidden]),
                    np.array([rnn_cell_values])]
    rnn_out = rnn.forward.predict(input_to_rnn)
    
    y_pred = rnn_out[0][0][0]
    rnn_hidden = rnn_out[1][0]
    rnn_cell_values = rnn_out[2][0]
    
    return np.concatenate([vae_encoded_obs, rnn_hidden]), rnn_hidden, rnn_cell_values


"""
Train the DQN to either the number of specified episodes or convergence (based on arguments).
"""
def train(controller_save, vae_rnn_file=None, controller_file=None, env='VizdoomTakeCover-v0'):
    vae = VAE()
    vae_path = f'./savedModels/vae/{vae_rnn_file}'
    print(f'Loading VAE from {vae_path}', flush=True)
    vae.set_weights(vae_path)

    rnn = RNN()
    rnn_path = f'./savedModels/rnn/{vae_rnn_file}'
    print(f'Loading RNN from {rnn_path}', flush=True)
    rnn.set_weights(rnn_path)

    state_size = ((rnn.hidden_units + rnn.z_dim))
    env = gym.make(env)
    print('Making environment {}'.format(env), flush=True)
    agent = DQNAgent(state_size, env.action_space.n)

    if controller_file:
        controller_path = f'./savedModels/controller/{controller_file}'
        print(f'Loading Controller from {controller_path}', flush=True)
        agent.load_model(controller_path)
    else:
        print('Using new controller', flush=True)

    convergence_threshold = 0.001   # The threshold which the running average must pass to be considered converged
    convergence_count = 0       # The number of episodes in a row the average reward has been below the convergence parameter
    total_reward = 0            # The total reward across all episodes
    average_reward = 0          # The rolling average reward across all episodes
    last_average = 0            # Rolling average at the previous episode (for testing convergence)
    episode_rewards = []        # The rewards by episode
    Q_loss = []
    total_step = 0              # The total number of steps taken across all episodes (needed to track when to train the model.)
    max_step = 1500
    episode = 0
    avg_Q_loss = 0

    rnn_hidden = np.zeros(rnn.hidden_units)
    rnn_cell_values = np.zeros(rnn.hidden_units)
    
    while args.converge or episode < args.max_eps:

        if args.render:
            env.render()
        
        episode += 1
        episode_reward = 0
        
        obs = env.reset()
        reward = 0
        action_idx = 0
        
        controller_obs, rnn_hidden, rnn_cell_values = get_obs_encoding(vae, rnn, obs, action_idx, reward, rnn_hidden, rnn_cell_values)

        for t_step in range(max_step):
            total_step += 1

            action_idx = agent.get_action(controller_obs[np.newaxis,:], training=False)

            try:
                next_obs, reward, done, info = env.step(action_idx)
                next_obs, rnn_hidden, rnn_cell_values = get_obs_encoding(vae, rnn, next_obs, action_idx, reward, rnn_hidden, rnn_cell_values)
            except Exception as e:
                print(e, flush=True)

                print("Environment crashed. Restarting\n", flush=True)
                total_reward += episode_reward
                break

            if args.render: 
                env.render()

            agent.replay_memory(controller_obs, action_idx, reward, next_obs, done)

            if total_step > agent.observe and total_step % agent.timestep_per_train == 0:
                epsilon, Qs, loss = agent.train_replay(total_step)
                print(total_step, epsilon, Qs, loss, flush=True)
                Q_loss.append(loss)
        
            controller_obs = next_obs
            episode_reward += reward

            if t_step == max_step or done:
                agent.update_target_model()
                total_reward += episode_reward
                episode_rewards.append(episode_reward)
                break
        
        if episode%10 == 0:
            agent.save_model(f'{controller_save}.h5')

        # Check change in rolling average for convergence and report
        if len(episode_rewards) > 1:
            average_reward = np.mean(episode_rewards[:-1])

        if len(Q_loss) > 1:
            avg_Q_loss = np.mean(Q_loss)
        
        report_training(controller_save, episode, episode_reward, total_reward, 
                        average_reward, last_average, avg_Q_loss)

        if args.converge and total_step > agent.observe and abs(average_reward - last_average) < convergence_threshold:
            if convergence_count < 5:
                print("Convergence count upped! Current count {}".format(convergence_count+1), flush=True)
                convergence_count += 1
                report_training(controller_save, episode, episode_reward, total_reward, 
                                average_reward, last_average, avg_Q_loss, no_append=True)
            else:
                print('Converged! Ending sequence.', flush=True)
                break
        else:
            convergence_count = 0

        if average_reward > last_average:
            agent.save_model(f'{controller_save}_best.h5')

        last_average = average_reward

    agent.save_model(f'{controller_save}.h5')
    env.close()
    
def test(training_id, vae_rnn_file, controller_file, num_eps=2500, env='VizdoomTakeCover-v0'):
    vae = VAE()
    vae_path = f'./savedModels/vae/{vae_rnn_file}'
    print(f'Loading VAE from {vae_path}', flush=True)
    vae.set_weights(vae_path)

    rnn = RNN()
    rnn_path = f'./savedModels/rnn/{vae_rnn_file}'
    print(f'Loading RNN from {rnn_path}', flush=True)
    rnn.set_weights(rnn_path)

    state_size = ((rnn.hidden_units + rnn.z_dim))
    env = gym.make(env)
    agent = DQNAgent(state_size, env.action_space.n)

    controller_path = f'./savedModels/controller/{controller_file}'
    print(f'Loading Controller from {controller_path}', flush=True)
    agent.load_model(controller_path)

    controller_save = controller_file.split('.')[0] + "_new_eval.log"

    rnn_hidden = np.zeros(rnn.hidden_units)
    rnn_cell_values = np.zeros(rnn.hidden_units)

    total_step = 0
    total_reward = 0
    average_reward = 0
    last_average = 0
    episode_rewards = []

    for episode in range(num_eps):
        episode_reward = 0
        done = False
        t_step = 0

        obs = env.reset()
        reward = 0
        action_idx = 0
        
        controller_obs, rnn_hidden, rnn_cell_values = get_obs_encoding(vae, rnn, obs, action_idx, reward, rnn_hidden, rnn_cell_values)

        while not done:
            total_step += 1

            action_idx = agent.get_action(controller_obs[np.newaxis,:], training=False)

            try:
                next_obs, reward, done, info = env.step(action_idx)
                next_obs, rnn_hidden, rnn_cell_values = get_obs_encoding(vae, rnn, next_obs, action_idx, reward, rnn_hidden, rnn_cell_values)
            except Exception as e:
                print(e, flush=True)

                print("Environment crashed. Restarting\n", flush=True)
                total_reward += episode_reward
                break

            agent.replay_memory(controller_obs, action_idx, reward, next_obs, done)

            controller_obs = next_obs
            episode_reward += reward

        total_reward += episode_reward
        episode_rewards.append(episode_reward)

        if len(episode_rewards) > 1:
            average_reward = np.mean(episode_rewards[:-1])

        report_training(controller_save, episode, episode_reward, total_reward, 
                        average_reward, last_average, '')

        last_average = average_reward

if __name__ == '__main__':
    # Change directory to appease the cluster gods.
    os.chdir('/nfs')

    parser = argparse.ArgumentParser(description='Run deep Q-network on the game VizDoom: Take Cover.')
    parser.add_argument('--train', dest='train', action='store_true', help='Train the model.')
    parser.add_argument('--max-eps', default=10000, type=int, help='Global maximum number of episodes to run.')
    parser.add_argument('--converge', dest='converge', action='store_true', help='Specify training to convergence as opposed to a set number of episodes.')
    parser.add_argument('--lr', default=0.0002, help='Learning rate for the shared optimizer.')
    parser.add_argument('--update-freq', default=20, type=int, help='How often to update the target network.')
    parser.add_argument('--gamma', default=0.99, help='Discount factor of rewards.')
    parser.add_argument('--report-step', default=10, help='Number of time steps between reporting.')
    parser.add_argument('--save-dir', default='/nfs/savedModels/controller', type=str, help='Directory in which to save the model.')
    parser.add_argument('--render', dest='render', action='store_true', help='Render the game enviornment (does not work in docker container).')
    
    parser.add_argument('--vae_rnn_file', default='weights.h5')
    parser.add_argument('--controller')
    parser.add_argument('--controller_save')
    parser.add_argument('--env')
    args = parser.parse_args()

    training_id = datetime.now().strftime('%d%m%Y-%H%M%S')

    if args.train:
        if not args.controller_save and args.controller:
            iter_num = int(args.controller.split('_')[3].split('.')[0])
            controller_save = f"wm_dqn_online_{iter_num+1}"
        elif args.controller_save:
            controller_save = args.controller_save
        else:
            print('Must supply controller file, save location, or both.', flush=True)
            exit(1)

        with open(f'{controller_save}.log', 'w') as file:
            file.write("Beginning log for training: {}\n".format(training_id))
        train(controller_save, args.vae_rnn_file, args.controller, args.env)
    else:
        test(training_id, args.vae_rnn_file, args.controller, args.env)