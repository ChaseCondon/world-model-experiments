# based on code from: https://github.com/flyyufelix/VizDoom-Keras-RL/blob/master/ddqn.py
import random

import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.layers as kl
import numpy as np

from ..ReplayBuffer import ReplayBuffer

class DQNAgent():
    """
    The Deep Q-Network 
    (without Convolutional Layers for use with the World Model)
    """

    def __init__(self, state_size, 
                 action_size,
                 gamma=0.99,
                 learning_rate=0.0005,
                 batch_size=32):
        self.state_size = state_size
        self.action_size = action_size 

        self.gamma = gamma
        self.learning_rate = learning_rate
        self.epsilon = 1.0
        self.initial_epsilon = 1.0
        self.epsilon_decay = .999
        self.final_epsilon = 0.01
        self.observe = 50000
        self.timestep_per_train = 100
        self.batch_size = batch_size
        
        self.replay_buffer = ReplayBuffer(self.observe)
        self.buffer_min_size = 10000

        self.model = self.create_model(self.state_size, self.action_size, self.learning_rate)
        self.target_model = self.create_model(self.state_size, self.action_size, self.learning_rate)


    def create_model(self, input_shape, action_size, learning_rate):
        model = keras.Sequential()
        model.add(kl.Dense(1024, input_shape=(input_shape,), activation='relu',
                                kernel_initializer=keras.initializers.GlorotNormal()))
        model.add(kl.Dense(256, activation='relu',
                                kernel_initializer=keras.initializers.GlorotNormal()))
        model.add(kl.Dense(action_size, activation='linear',
                                kernel_initializer=keras.initializers.GlorotNormal()))

        adam = tf.keras.optimizers.Adam(lr=learning_rate)
        model.compile(loss=keras.losses.Huber(), optimizer=adam)

        return model


    def update_target_model(self):
        """
        Updates the target network by setting the weights equal to the primary network
        """
        self.target_model.set_weights(self.model.get_weights())


    def get_action(self, state, training=True):
        """
        Get action from model using epsilon-greedy policy
        """
        if training:
            if np.random.rand() <= self.epsilon:
                action_idx = random.randrange(self.action_size)
            else:
                q = self.model.predict(state)
                action_idx = np.argmax(q)
        else:
            q = self.model.predict(state)
            action_idx = np.argmax(q)
        return action_idx
           

    def replay_memory(self, s_t, action_idx, r_t, s_t1, is_terminated):
        self.replay_buffer.add(s_t, action_idx, r_t, is_terminated, s_t1)


    def train_replay(self, t_step):

        num_samples = min(self.batch_size * self.timestep_per_train, self.replay_buffer.size)
        replay_samples = self.replay_buffer.sample(num_samples)

        states_mb = np.array([each[0] for each in replay_samples], ndmin=2)
        actions_mb = np.array([each[1] for each in replay_samples])
        rewards_mb = np.array([each[2] for each in replay_samples])
        next_states_mb = np.array([each[4] for each in replay_samples], ndmin=2)
        done_mb = np.array([each[3] for each in replay_samples])

        target_Qs_batch = self.model.predict(states_mb)
        Qs_next_state = self.target_model.predict(next_states_mb)

        for i in range(num_samples):
            if done_mb[i]:
                target_Qs_batch[i][actions_mb[i]] = rewards_mb[i]
            else:
                target = rewards_mb[i] + self.gamma * np.max(Qs_next_state[i])
                target_Qs_batch[i][actions_mb[i]] = target

        targets_mb = np.array([each for each in target_Qs_batch])
        loss = self.model.fit(states_mb, targets_mb, batch_size=self.batch_size, epochs=1, verbose=0)

        if self.epsilon > self.final_epsilon and t_step > self.observe:
            self.epsilon *= self.epsilon_decay

        return self.epsilon, target_Qs_batch[-1], loss.history['loss']
   
   
    def load_model(self, name):
        self.model.load_weights(name)
    
    
    def save_model(self, name):
        self.model.save_weights(name)