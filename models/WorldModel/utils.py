from datetime import datetime

def log(msg):
    print("{timestamp} - {msg}".format(
        timestamp = datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
        msg = msg
    ), flush=True)