import numpy as np 
import tensorflow as tf 
import tensorflow.keras.layers as kl

class ProbabilityDistribution(tf.keras.Model):
    def call(self, logits, **kwargs):
        return tf.squeeze(tf.random.categorical(logits, 1), axis=-1)

class ActorCritic(tf.keras.Model):
    def __init__(self, input_shape, num_actions):
        super().__init__('mlp_policy')

        # self.input = kl.Input((input_shape))
        self.conv1 = kl.Conv2D(32, (8, 8), strides=(4, 4), activation='relu', padding = 'same', 
                            kernel_initializer = 'he_normal')
        self.conv2 = kl.Conv2D(64, (4, 4), strides=(2, 2), activation='relu', padding = 'same', 
                            kernel_initializer = 'he_normal')
        self.conv3 = kl.Conv2D(54, (3, 3), activation='relu', padding = 'same', 
                            kernel_initializer = 'he_normal')                    
        self.pool = kl.MaxPooling2D(pool_size=(2,2))
        self.flatten = kl.Flatten()

        self.dense1 = kl.Dense(128, activation='relu')
        self.dense2 = kl.Dense(128, activation='relu')
        
        self.value = kl.Dense(1, name='value')
        self.logits = kl.Dense(num_actions, name='policy_logits')
        self.dist = ProbabilityDistribution()

    def call(self, inputs, **kwargs):
        x = tf.convert_to_tensor(inputs)
        hidden_conv1 = self.pool(self.conv1(x))
        hidden_conv2 = self.pool(self.conv2(hidden_conv1))
        hidden_conv3 = self.pool(self.conv3(hidden_conv2))
        hidden_flat = self.flatten(hidden_conv3)

        hidden_logs = self.dense1(hidden_flat)
        hidden_vals = self.dense2(hidden_flat)

        return self.logits(hidden_logs), self.value(hidden_vals)

    def action_value(self, obs):
        logits, value = self.predict_on_batch(obs)
        action = self.dist.predict_on_batch(logits)

        return np.squeeze(action, axis=-1), np.squeeze(value, axis=-1)