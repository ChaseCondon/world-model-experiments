from collections import deque

import gym
import vizdoomgym
import numpy as np
import skimage as skimage
from skimage import transform, color, exposure

def preprocessImg(frame, 
                  size=(84,84)):
    """
    Preprocesses an image to a grayscale of the specified size.
    """
    gray_frame = np.dot(frame[..., :3] , [0.299, 0.587, 0.114])
    crop_frame = gray_frame[30:-30,30:-30]
    normalized_frame = crop_frame/255.0
    preprocessed_frame = transform.resize(normalized_frame, [84,84])
    return preprocessed_frame


def stack_frames(stacked_frames, stack_size, state, is_reset=False, append=True):
    """
    Add frames to the current frame stack
    """
    frame = preprocessImg(state)
    
    if is_reset:
        stacked_frames = deque([np.zeros((84,84), dtype=np.int) for i in range(stack_size)], maxlen=4)
        
        stacked_frames.append(frame)
        stacked_frames.append(frame)
        stacked_frames.append(frame)
        stacked_frames.append(frame)
        
        stacked_state = np.stack(stacked_frames, axis=2)
        
    else:
        if append:
            stacked_frames.append(frame)

        stacked_state = np.stack(stacked_frames, axis=2) 
    
    return stacked_state, stacked_frames

class Environment(object):
    """ Environment Helper Class (Multiple State Buffer) for Continuous Action Environments
    (MountainCarContinuous-v0, LunarLanderContinuous-v2, etc..), and MujuCo Environments
    """
    def __init__(self, gym_env, action_repeat):
        self.env = gym_env
        self.timespan = action_repeat
        self.gym_actions = range(gym_env.action_space.n)
        self.state_buffer = deque([np.zeros((84,84), dtype=np.int) for i in range(action_repeat)], maxlen=4)

    def get_action_size(self):
        return self.env.action_space.n

    def get_state_size(self):
        return (84, 84, self.timespan)

    def reset(self):
        """ Resets the game, clears the state buffer.
        """
        # Clear the state buffer
        self.state_buffer = deque()
        x_t = self.env.reset()
        s_t, self.state_buffer = stack_frames(self.state_buffer, self.timespan, x_t, True)
        return s_t

    def step(self, action):
        x_t1, r_t, terminal, info = self.env.step(action)
        s_t1, self.state_buffer = stack_frames(self.state_buffer, self.timespan, x_t1)
        return s_t1, r_t, terminal, info

    def render(self):
        return self.env.render()
