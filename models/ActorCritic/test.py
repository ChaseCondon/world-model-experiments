import gym
import vizdoomgym
from environment import Environment
from model import Model
from A2C import A2CAgent

if __name__ == '__main__':
    env = Environment(gym.make('VizdoomTakeCover-v0'), 4)
    model = Model(env.get_state_size(), env.get_action_size())
    agent = A2CAgent(model)

    agent.train(env)
