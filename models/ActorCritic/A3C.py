import time
from threading import Thread, Lock
from datetime import datetime

import numpy as np
import tensorflow as tf
import tensorflow.keras.losses as kls
import tensorflow.keras.optimizers as ko

from .models import ActorCritic
from .environment import Environment

episode = 0
lock = Lock()

def report_training(file_path,
                    episode,
                    episode_reward,
                    total_reward,
                    average_reward,
                    last_average,
                    no_append=False,
                    report_step=10):
    """
    Report on training stats
    """
    report = "{date} - Episode {episode}:\
               \n\tlatest episode reward: {episode_reward}\
               \n\ttotal episode reward: {total_reward}\
               \n\taverage_reward: {average_reward}\
               \n\tchange in average: {average_change}".format(
                    date = datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
                    episode = episode,
                    episode_reward = episode_reward,
                    total_reward = total_reward,
                    average_reward = average_reward,
                    average_change = (average_reward - last_average),
                )

    if episode%report_step == 0:
        print(report, flush=True)
    if not no_append:
        with open(file_path, 'a') as file:
            file.write(report + '\n')

class A3CAgent:
    def __init__(self, input_shape, num_actions, num_threads=8, lr=7e-3, gamma=0.99, value_c=0.5, entropy_c=1e-4):
        self.gamma = gamma
        self.n_threads = num_threads
        
        self.value_c = value_c 
        self.entropy_c = entropy_c

        self.model = ActorCritic(input_shape, num_actions)
        self.model.compile(
            optimizer=ko.RMSprop(lr=lr),
            loss=[self._logits_loss, self._value_loss])

    def _value_loss(self, returns, value):
        # Value loss is typically MSE between value estimates and returns.
        return self.value_c * kls.mean_squared_error(returns, value)
    
    def _logits_loss(self, actions_and_advantages, logits):
        # A trick to input actions and advantages through the same API.
        actions, advantages = tf.split(actions_and_advantages, 2, axis=-1)

        # Sparse categorical CE loss obj that supports sample_weight arg on `call()`.
        # `from_logits` argument ensures transformation into normalized probabilities.
        weighted_sparse_ce = kls.SparseCategoricalCrossentropy(from_logits=True)

        # Policy loss is defined by policy gradients, weighted by advantages.
        # Note: we only calculate the loss on the actions we've actually taken.
        actions = tf.cast(actions, tf.int32)
        policy_loss = weighted_sparse_ce(actions, logits, sample_weight=advantages)

        # Entropy loss can be calculated as cross-entropy over itself.
        probs = tf.nn.softmax(logits)
        entropy_loss = kls.categorical_crossentropy(probs, probs)

        # We want to minimize policy and maximize entropy losses.
        # Here signs are flipped because the optimizer minimizes.
        return policy_loss - self.entropy_c * entropy_loss

    def train(self, env, batch_sz=64, max_episode=5000):
        # SetUp for logging and storage
        training_id = datetime.now().strftime('%d%m%Y-%H%M%S')
        file_path = "a3c-out-{}.log".format(training_id)
        with open(file_path, 'w') as file:
            file.write("Beginning log for training: {}\n".format(training_id))

        envs = [Environment(env, 4) for i in range(self.n_threads)]
        [e.reset() for e in envs]
        state_dim = envs[0].get_state_size()
        action_dim = envs[0].get_action_size()

        global episode
        episode = 0
        # Create threads
        threads = [Thread(
                target=self.training_thread,
                daemon=True,
                args=(i,
                    envs[i],
                    batch_sz,
                    max_episode)) for i in range(self.n_threads)]

        for t in threads:
            t.start()
            time.sleep(0.5)
        try:
            [t.join() for t in threads]
        except KeyboardInterrupt:
            print("Exiting all threads...")
        return None

    def training_thread(self, name, env, batch_sz=64, max_episode=5000):
        global episode 

        while episode < max_episode:
            lock.acquire()
            print(name, episode)
            episode += 1
            lock.release()
            time.sleep(0.5)