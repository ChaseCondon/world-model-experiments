import os
import argparse 

import numpy as np 
import tensorflow as tf 

from utils import log
from models.WorldModel.RNN import RNN


ROOT_DIR_NAME = './data/'
SERIES_DIR_NAME = './data/series/'


def get_filelist(N, series_dir):
    filelist = os.listdir(series_dir)
    filelist = [x for x in filelist if x != '.DS_Store']
    filelist.sort()
    length_filelist = len(filelist)


    if length_filelist > N:
        filelist = filelist[:N]

    if length_filelist < N:
        N = length_filelist

    max_len = -float("inf")

    for fl in filelist:
        data = np.load(series_dir + fl)
        if len(data['action']) > max_len:
            max_len = len(data['action'])

    return filelist, N, max_len


def random_batch(filelist, batch_size, series_dir):
    N_data = len(filelist)
    indices = np.random.permutation(N_data)[0:batch_size]

    z_list = []
    action_list = []
    rew_list = []
    done_list = []

    for i in indices:
        try:
            new_data = np.load(series_dir + filelist[i])

            mu = new_data['mu']
            log_var = new_data['log_var']
            action = new_data['action']
            reward = new_data['reward']
            done = new_data['done']

            action = np.expand_dims(action, axis=1)
            reward = np.expand_dims(reward, axis=1)
            done = np.expand_dims(done, axis=1)

            s = log_var.shape

            z = mu + np.exp(log_var/2.0) * np.random.randn(*s)

            z_list.append(z)
            action_list.append(action)
            rew_list.append(reward)
            done_list.append(done)
        except Exception as e:
            log(e)

    z_list = np.array(z_list)
    action_list = np.array(action_list)
    rew_list = np.array(rew_list)
    done_list = np.array(done_list)

    return z_list, action_list, rew_list, done_list

def train_rnn(N=1000, save_folder=None, new_model=True, rnn_file=None, steps=500, batch_size=100):
    N = int(N)
    steps = int(steps)
    batch_size = int(batch_size)

    # tf.compat.v1.disable_eager_execution()
    rnn = RNN() #learning_rate = LEARNING_RATE

    if save_folder:
        root_dir = ROOT_DIR_NAME + save_folder + '/'
        series_dir = SERIES_DIR_NAME + save_folder + '/'
    
    if rnn_file:
        model_save_path = './savedModels/rnn/' + rnn_file
    else:
        model_save_path = './savedModels/rnn/weights.h5'

    if not new_model:
        try:
            rnn.set_weights(model_save_path)
        except:
            log(f"Either set --new_model or ensure {model_save_path} exists")
            raise

    log(f'root dir: {root_dir}')
    log(f'series dir: {series_dir}')
    log(f'model path: {model_save_path}')

    log(f'Gathering data')
    filelist, N, max_size = get_filelist(N, series_dir)

    log(f'Maximum episode length {max_size}')

    for step in range(steps):
        log('STEP ' + str(step))

        z, action, rew, done = random_batch(filelist, batch_size, series_dir)

        rnn_input = list()
        rnn_output = list()
        # max_len = 0

        for i in range(batch_size):
            # if len(z[i]) > max_len:
            #     max_len = len(z[i])
            rnn_input.append(np.concatenate([z[i][:-1, :], action[i][:-1, :], rew[i][:-1, :]], axis=1))
            rnn_output.append(np.concatenate([z[i][1:, :], done[i][1:, :], rew[i][1:, :]], axis=1))
        
        # rnn_input = tf.keras.preprocessing.sequence.pad_sequences(np.array(rnn_input),
        #                 maxlen=max_size, padding='post', value=0.0, dtype='float64') 
        # rnn_output = tf.keras.preprocessing.sequence.pad_sequences(np.array(rnn_output),
        #                 maxlen=max_size, padding='post', value=0.0, dtype='float64') 

        # log(max_len)
        # log(rnn_input.shape)
        # log(rnn_output.shape)

        if step == 0:
            np.savez_compressed(root_dir + 'rnn_files.npz', rnn_input = rnn_input, rnn_output = rnn_output)

        for i in range(len(rnn_input)):
            rnn.train(rnn_input[i][np.newaxis,:], rnn_output[i][np.newaxis,:])

        # rnn.train(rnn_input, rnn_output)
 
        if step % 10 == 0:

            rnn.model.save_weights(model_save_path)

    rnn.model.save_weights(model_save_path)




if __name__ == "__main__":
    os.chdir('/nfs')
    
    parser = argparse.ArgumentParser(description=('Train RNN'))
    parser.add_argument('--N', type=int, default=1000, help='number of episodes to use to train')
    parser.add_argument('--new_model', action='store_true', help='start a new model from scratch?')
    parser.add_argument('--steps', type=int, default = 1000, help='how many rnn batches to train over')
    parser.add_argument('--batch_size', type=int, default = 100, help='how many episodes in a batch?')
    parser.add_argument('--save_folder', help='the subfolder containing the training data')
    parser.add_argument('--rnn_file', help='The file to save the RNN in')

    args = parser.parse_args()

    train_rnn(args.N, args.save_folder, args.new_model, args.rnn_file, args.steps, args.batch_size)