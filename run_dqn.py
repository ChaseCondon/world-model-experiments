import os
import pickle
import resource
import argparse
from collections import deque
from datetime import datetime

import gym
import vizdoomgym
import numpy as np
import tensorflow as tf
import skimage as skimage
from skimage import transform, color, exposure

from models.DQN import DQNAgent
from models.ReplayBuffer import ReplayBuffer

logs = []

parser = argparse.ArgumentParser(description='Run deep Q-network on the game VizDoom: Take Cover.')
parser.add_argument('--train', dest='train', action='store_true', help='Train the model.')
parser.add_argument('--max-eps', default=10000, type=int, help='Global maximum number of episodes to run.')
parser.add_argument('--converge', dest='converge', action='store_true', help='Specify training to convergence as opposed to a set number of episodes.')
parser.add_argument('--lr', default=0.0002, help='Learning rate for the shared optimizer.')
parser.add_argument('--update-freq', default=20, type=int, help='How often to update the target network.')
parser.add_argument('--gamma', default=0.99, help='Discount factor of rewards.')
parser.add_argument('--report-step', default=10, help='Number of time steps between reporting.')
parser.add_argument('--save-dir', default='/nfs/saved_models/', type=str, help='Directory in which to save the model.')
parser.add_argument('--render', dest='render', action='store_true', help='Render the game enviornment (does not work in docker container).')
parser.add_argument('--pickle', dest='pickle', action='store_true')
parser.add_argument('--controller_file')
parser.add_argument('--controller_save', type=str)
parser.add_argument('--env', default='VizdoomTakeCover-v0', type=str)
args = parser.parse_args()

"""
Report on training stats
"""
def report_training(controller_save,
                    episode,
                    episode_reward,
                    total_reward,
                    average_reward,
                    last_average,
                    avg_Q_loss,
                    no_append=False):
    report = "{date} - Episode {episode}:\
               \n\tlatest episode reward: {episode_reward}\
               \n\ttotal episode reward: {total_reward}\
               \n\taverage_reward: {average_reward}\
               \n\tchange in average: {average_change}\
               \n\taverage Q_loss: {avg_Q_loss}".format(
                    date = datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
                    episode = episode,
                    episode_reward = episode_reward,
                    total_reward = total_reward,
                    average_reward = average_reward,
                    average_change = (average_reward - last_average),
                    avg_Q_loss = avg_Q_loss
                )

    if (episode-1)%args.report_step == 0:
        print(report, flush=True)
    if not no_append:
        with open("{}.log".format(controller_save), 'a') as file:
            file.write(report + '\n')

"""
Preprocesses an image to a grayscale of the specified size.
"""
def preprocessImg(frame, 
                  size=(84,84)):
    gray_frame = np.dot(frame[..., :3] , [0.299, 0.587, 0.114])
    if args.env == 'VizdoomTakeCover-v0':
        crop_frame = gray_frame[80:-10,30:-30]
    elif args.env == 'CartPole-v0':
        crop_frame = gray_frame[150:,:]
    elif args.env == 'MsPacman-v0':
        crop_frame = gray_frame[172:,:]
    else:
        crop_frame = gray_frame[25:-10,:]
    normalized_frame = crop_frame/255.0
    preprocessed_frame = transform.resize(normalized_frame, [84,84])
    return preprocessed_frame


"""
Add frames to the current frame stack
"""
def stack_frames(stacked_frames, stack_size, state, is_new_episode, append=True):
    frame = preprocessImg(state)
    
    if is_new_episode:
        stacked_frames = deque([np.zeros((84,84), dtype=np.int) for i in range(stack_size)], maxlen=4)
        
        stacked_frames.append(frame)
        stacked_frames.append(frame)
        stacked_frames.append(frame)
        stacked_frames.append(frame)
        
        stacked_state = np.stack(stacked_frames, axis=2)
        
    else:
        if append:
            stacked_frames.append(frame)

        stacked_state = np.stack(stacked_frames, axis=2) 
    
    return stacked_state, stacked_frames

def generate_stack(num_eps=5, save_file="saved_stack.pk"):
    img_rows, img_cols = 84, 84
    stack_size = 4
    frame_skip = 2
    buffer = deque()
    stacked_frames = deque([np.zeros((img_rows, img_cols), dtype=np.int) for i in range(stack_size)], maxlen=4) 

    print('Making environment for {}'.format(args.env), flush=True)
    env = gym.make(args.env)

    for ep in range(num_eps):
        state = env.reset()
        done = False

        print(ep+1)

        for t_step in range(10000):
            stacked_state, stacked_frames = stack_frames(stacked_frames, stack_size, state, t_step == 0, False)

            action_idx = env.action_space.sample()
            for i in range(frame_skip):
                next_state, _, done, _ = env.step(action_idx)
                if done: break

            stacked_state_, stacked_frames = stack_frames(stacked_frames, stack_size, next_state, t_step == 0)
            buffer.append((stacked_state, 0, 0, done, stacked_state_))

            state = next_state

            if done:
                break

    with open(save_file, 'wb') as file:
        pickle.dump(buffer, file)



"""
Train the DQN to either the number of specified episodes or convergence (based on arguments).
"""
def train(controller_save):
    img_rows, img_cols = 84, 84
    stack_size = 4
    stacked_frames = deque([np.zeros((img_rows, img_cols), dtype=np.int) for i in range(stack_size)], maxlen=4) 
    state_size = (img_rows, img_cols, stack_size)

    # Change directory to appease the cluster gods.
    # os.chdir('/nfs')
    print('Making environment for {}'.format(args.env), flush=True)
    env = gym.make(args.env)
    agent = DQNAgent(state_size, env.action_space.n)

    convergence_threshold = 0.001   # The threshold which the running average must pass to be considered converged
    convergence_count = 0       # The number of episodes in a row the average reward has been below the convergence parameter
    total_reward = 0            # The total reward across all episodes
    average_reward = 0          # The rolling average reward across all episodes
    last_average = 0            # Rolling average at the previous episode (for testing convergence)
    episode_rewards = []        # The rewards by episode
    Q_loss = []
    total_step = 0              # The total number of steps taken across all episodes (needed to track when to train the model.)
    max_step = 5000
    episode = 0
    avg_Q_loss = 0

    
    while args.converge or episode < args.max_eps:

        episode += 1
        episode_reward = 0
        state = env.reset()

        if args.env == 'CartPole-v0':
            state = env.render(mode='rgb_array')
            assert state.shape == (400, 600, 3)

        if args.render:
            env.render()

        for t_step in range(max_step):
            total_step += 1

            is_new_episode = t_step == 0
            stacked_state, stacked_frames = stack_frames(stacked_frames, stack_size, state, t_step == 0, False)

            action_idx = agent.get_action(stacked_state[np.newaxis,:])

            try:
                next_state, reward, done, info = env.step(action_idx)
                if args.env == 'CartPole-v0':
                    next_state = env.render(mode='rgb_array')
                    assert next_state.shape == (400, 600, 3)
            except:
                print("Env crashed. Restarting\n", flush=True)
                total_reward += episode_reward
                break

            if args.render: 
                env.render()

            stacked_state_, stacked_frames = stack_frames(stacked_frames, stack_size, next_state, t_step == 0)
            agent.replay_memory(stacked_state, action_idx, reward, stacked_state_, done)

            if args.converge and total_step > agent.observe and total_step % agent.timestep_per_train == 0:
                epsilon, Qs, loss = agent.train_replay(total_step)
                print(total_step, epsilon, Qs, loss, flush=True)
                Q_loss.append(loss)
        
            state = next_state
            episode_reward += reward

            if t_step == max_step or done:
                agent.update_target_model()
                total_reward += episode_reward
                episode_rewards.append(episode_reward)
                break
        
        if episode%10 == 0:
            agent.save_model("{}.h5".format(controller_save))

        # Check change in rolling average for convergence and report
        if len(episode_rewards) > 1:
            average_reward = np.mean(episode_rewards[:-1])

        if len(Q_loss) > 1:
            avg_Q_loss = np.mean(Q_loss)
        
        report_training(controller_save, episode, episode_reward, total_reward, 
                        average_reward, last_average, avg_Q_loss)

        if args.converge and total_step > agent.observe and abs(average_reward - last_average) < convergence_threshold:
            if convergence_count < 5:
                print("Convergence count upped! Current count {}".format(convergence_count+1), flush=True)
                convergence_count += 1
                report_training(controller_save, episode, episode_reward, total_reward, 
                                average_reward, last_average, avg_Q_loss, no_append=True)
            else:
                print('Converged! Ending sequence.', flush=True)
                break
        else:
            convergence_count = 0

        if average_reward > last_average:
            agent.save_model("{}_best.h5".format(controller_save))

        last_average = average_reward

    agent.save_model("{}.h5".format(controller_save))
    env.close()
    

def test(training_id, controller_file, num_eps=2500):
    img_rows, img_cols = 84, 84
    stack_size = 4
    stacked_frames = deque([np.zeros((img_rows, img_cols), dtype=np.int) for i in range(stack_size)], maxlen=4) 
    state_size = (img_rows, img_cols, stack_size)

    print('Making environment for {}'.format(args.env), flush=True)
    env = gym.make(args.env)
    agent = DQNAgent(state_size, env.action_space.n)

    try:
        print(f'Loading controller from {controller_file}', flush=True)
        agent.load_model(controller_file)
    except Exception as e:
        print(e, flush=True)
        return

    total_step = 0
    total_reward = 0
    average_reward = 0
    last_average = 0
    episode_rewards = []

    for episode in range(num_eps):
        episode_reward = 0
        state = env.reset()
        done = False
        t_step = 0

        while not done:
            total_step += 1

            is_new_episode = t_step == 0
            stacked_state, stacked_frames = stack_frames(stacked_frames, stack_size, state, t_step == 0, False)

            action_idx = agent.get_action(stacked_state[np.newaxis,:])

            try:
                next_state, reward, done, info = env.step(action_idx)
            except:
                print("Env crashed. Restarting\n", flush=True)
                # total_reward += episode_reward
                break

            stacked_state_, stacked_frames = stack_frames(stacked_frames, stack_size, next_state, t_step == 0)

            state = next_state
            episode_reward += reward

        total_reward += episode_reward
        episode_rewards.append(episode_reward)

        if len(episode_rewards) > 1:
            average_reward = np.mean(episode_rewards[:-1])

        report_training(training_id, episode, episode_reward, total_reward, 
                        average_reward, last_average, '')

        last_average = average_reward


if __name__ == '__main__':
    os.chdir('/nfs')
    training_id = datetime.now().strftime('%d%m%Y-%H%M%S')

    if not args.controller_save and args.controller:
        iter_num = int(args.controller.split('_')[3].split('.')[0])
        controller_save = f"wm_dqn_online_{iter_num+1}"
    elif args.controller_save:
        controller_save = args.controller_save
    else:
        print('Must supply controller file, save location, or both.', flush=True)
        exit(1)

    with open("{}.log".format(controller_save), 'w') as file:
        file.write("Beginning log for training: {}\n".format(training_id))

    if args.train:
        train(controller_save)
    elif args.pickle:
        generate_stack()
    else:
        if args.controller_file:
            test(controller_save, args.controller_file)
        else:
            print('Need to specify a controller file with --controller_file to test')
