import os
import time
import random
import argparse
import threading

import gym
import vizdoomgym
import numpy as np

import config
from utils import log
from models.WorldModel.VAE import VAE
from models.WorldModel.RNN import RNN
from models.WorldModel.DQN import DQNAgent

DIR_NAME = './data/rollout/'
MODELS_DIR = './savedModels/'
lock = threading.Lock()
episode = 0

def get_obs_encoding(vae, rnn, obs, action, reward, rnn_hidden, rnn_cell_values):
    obs = config.adjust_obs(obs)
    vae_encoded_obs = vae.encoder.predict(np.array([obs]))[0]

    action = np.array([action])
    input_to_rnn = [np.array([[np.concatenate([vae_encoded_obs, action, [reward]])]]),
                    np.array([rnn_hidden]),
                    np.array([rnn_cell_values])]
    rnn_out = rnn.forward.predict(input_to_rnn)
    
    y_pred = rnn_out[0][0][0]
    rnn_hidden = rnn_out[1][0]
    rnn_cell_values = rnn_out[2][0]
    
    return np.concatenate([vae_encoded_obs, rnn_hidden]), rnn_hidden, rnn_cell_values


def worker(name, env, num_episodes, action_refresh_rate, episode_length, controller_file=None, save_folder=None):
    global episode

    log(f"Wkr {name}: starting.")

    if controller_file:
        log(f"Wkr {name}: Controller file specified. Loading pre-trained models...")

        log(f"Wkr {name}: Loading VAE...")
        vae = VAE()
        vae.set_weights(MODELS_DIR + 'vae/weights.h5')

        log(f"Wkr {name}: Loading RNN...")
        rnn = RNN()
        rnn.set_weights(MODELS_DIR + 'rnn/weights.h5')

        log(f"Wkr {name}: Loading Controller...")
        state_size = ((rnn.hidden_units + rnn.z_dim))
        controller = DQNAgent(state_size, env.action_space.n)
        controller.load_model(MODELS_DIR + 'controller/' + controller_file)

        log(f"Wkr {name}: Loading complete.")
        rnn_hidden = np.zeros(rnn.hidden_units)
        rnn_cell_values = np.zeros(rnn.hidden_units)
    else:
        controller = None
        

    while episode < num_episodes:
        with lock:
            episode += 1
            cur_ep = episode

        episode_id = random.randint(0, 2**31 - 1)
        if save_folder:
            filename = DIR_NAME + save_folder + '/' + str(episode_id) + ".npz"
        else:
            filename = DIR_NAME + str(episode_id) + ".npz"

        observation = env.reset()

        if args.env == 'CartPole-v0':
            observation = env.render(mode='rgb_array')
            assert observation.shape == (400, 600, 3)

        obs_sequence = []
        action_sequence = []
        reward_sequence = []
        done_sequence = []

        action = 0
        t = 0
        reward = 0
        done = False

        start_obs = config.adjust_obs(observation, args.env)
        obs_sequence.append(start_obs)
        action_sequence.append(action)
        reward_sequence.append(reward)
        done_sequence.append(done)

        while True:
            if t % action_refresh_rate == 0:
                if controller:
                    controller_obs, rnn_hidden, rnn_cell_values = get_obs_encoding(vae, rnn, observation, action, reward, rnn_hidden, rnn_cell_values)
                    action = controller.get_action(controller_obs[np.newaxis,:], training=False)
                else:
                    action = config.generate_data_action(t, env)

            observation = config.adjust_obs(observation, args.env)

            obs_sequence.append(observation)
            action_sequence.append(action)
            reward_sequence.append(reward)
            done_sequence.append(done)

            if done:
                break

            observation, reward, done, info = env.step(action)

            if args.env == 'CartPole-v0':
                observation = env.render(mode='rgb_array')
                assert observation.shape == (400, 600, 3)

            t = t + 1

        log("Wkr {wkr}: Episode {epi} finished after {ts} timesteps".format(wkr=name, 
                                                                            epi=cur_ep,
                                                                            ts=t))

        for obs in obs_sequence:
            assert obs.shape == (200,200,3)
        assert done_sequence[-1]
        assert not done_sequence[-2]

        np.savez_compressed(filename, obs=obs_sequence, action=action_sequence, 
                            reward=reward_sequence, done=done_sequence)
        
    env.close()

def generate_data(num_episodes=1000, action_refresh_rate = 3, num_threads=8, episode_length=1500, controller_file=None, save_folder=None, env='VizdoomTakeCover-v0'):
    envs = [gym.make(env) for i in range(num_threads)]
    log("Generating data for env {}".format(env))

    global episode
    episode = 0

    threads = [threading.Thread(target=worker, 
                                args=(i,
                                      envs[i],
                                      num_episodes, 
                                      action_refresh_rate, 
                                      episode_length,
                                      controller_file,
                                      save_folder))
                for i in range(num_threads)]

    log("Starting workers.")
    [threads[i].start() for i in range(num_threads)]

    time.sleep(1)

    [threads[i].join() for i in range(num_threads)]
    

if __name__ == "__main__":
    os.chdir('/nfs')

    parser = argparse.ArgumentParser(description=('Create new training data'))
    parser.add_argument('--total_episodes', type=int, default=1000,
                        help='total number of episodes to generate per worker')
    parser.add_argument('--action_refresh_rate', default=2, type=int,
                        help='how often to change the random action, in frames')
    parser.add_argument('--num_threads', type=int, default=8,
                        help='number of worker threads to generate data')
    parser.add_argument('--controller', help='the filename of the controller used to generate')
    parser.add_argument('--save_folder', help='where to save data')
    parser.add_argument('--env', default='VizdoomTakeCover-v0', type=str)

    args = parser.parse_args()
    generate_data(num_episodes=args.total_episodes, action_refresh_rate=args.action_refresh_rate, 
                    num_threads=args.num_threads, controller_file=args.controller, save_folder=args.save_folder, env=args.env)